<?php

declare(strict_types=1);

namespace App\Service\Producto;

use App\Exception\Producto;

final class ProductoService extends Base
{
    public function getAll(int $codpro ): array
    {
        return $this->productoRepository->getAll($codpro );
    }

    public function getOne(int $codpro)
    {
        $producto = $this->getOneFromDb($codpro);
        return $producto;
    }

    public function search(string $productosName): array
    {
        return $this->productoRepository->searchProductos($productosName);
    }

    public function create($input)
    {
        $data = json_decode(json_encode($input), false);
        if (!isset($data->nompro)) {
            throw new Producto('data inválida: nombre es requerido.', 400);
        }
        self::validateProductoName($data->nompro);
        $data->nompro = $data->nompro ?? null;
        $producto = $this->productoRepository->create($data);

        return $producto;
    }

    public function update($input, int $codpro)
    {
        $producto = $this->getOneFromDb($codpro);
        $data = json_decode(json_encode($input), false);
        if (!isset($data->nompro) && !isset($data->nompro)) {
            throw new Producto('Enter the data to update the producto.', 400);
        }
       
        if (isset($data->nompro)) {
            $producto->nompro = self::validateProductoName($data->nompro);
        }
       
        if (isset($data->precio)) {
            $producto->precio = $data->precio;
        }
       

        $productos = $this->productoRepository->update($producto);

        return $productos;
    }

    public function delete(int $codpro): string
    {
        $this->getOneFromDb($codpro);
        $data = $this->productoRepository->delete($codpro);
        return $data;
    }

    public function getCarrito(string $codpros): array
    {
        return $this->productoRepository->getCarrito($codpros);
    }
}
