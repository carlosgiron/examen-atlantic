<?php

declare(strict_types=1);

namespace App\Service\Producto;

use App\Exception\Producto;
use App\Repository\ProductoRepository;
use App\Service\BaseService;

use Respect\Validation\Validator as v;

abstract class Base extends BaseService
{
    private const REDIS_KEY = 'producto:%s';

    // protected ProductoRepository $productoRepository;

    // protected RedisService $redisService;

    public function __construct(ProductoRepository $productoRepository)
    {
        $this->productoRepository = $productoRepository;
    }

    protected static function validateProductoName(string $name): string
    {
        if (!v::length(2, 50)->validate($name)) {
            throw new Producto('El nombre del producto es inválido.', 400);
        }

        return $name;
    }

    protected function getOneFromCache(int $productoId)
    {
        $redisKey = sprintf(self::REDIS_KEY, $productoId);
        $key = $this->redisService->generateKey($redisKey);
        if ($this->redisService->exists($key)) {
            $producto = $this->redisService->get($key);
        } else {
            $producto = $this->getOneFromDb($productoId);
            $this->redisService->setex($key, $producto);
        }
        // $producto = $this->getOneFromDb($productoId);
        return $producto;
    }

    protected function getOneFromDb(int $productoId)
    {
        return $this->productoRepository->getProducto($productoId);
    }

    protected function saveInCache($id, $producto): void
    {
        $redisKey = sprintf(self::REDIS_KEY, $id);
        $key = $this->redisService->generateKey($redisKey);
        $this->redisService->setex($key, $producto);
    }

    protected function deleteFromCache($productoId): void
    {
        $redisKey = sprintf(self::REDIS_KEY, $productoId);
        $key = $this->redisService->generateKey($redisKey);
        $this->redisService->del($key);
    }
}
