<?php

declare(strict_types=1);

namespace App\Service\Categoria;

use App\Exception\Categoria;

final class CategoriaService extends Base
{
    public function getAll(): array
    {
        return $this->categoriaRepository->getAll();
    }

    public function getOne(int $codcat)
    {
        $categoria = $this->getOneFromDb($codcat);
        return $categoria;
    }
    public function getPorGrupo(int $idGrupo)
    {
        $categoria = $this->categoriaRepository->getPorGrupo($idGrupo);
        return $categoria;
    }
    public function search(string $categoriasName): array
    {
        return $this->categoriaRepository->searchCategorias($categoriasName);
    }



    public function create($input)
    {
        $data = json_decode(json_encode($input), false);
        if (!isset($data->nomcat)) {
            throw new Categoria('Data inválida: Nombre es requerido.', 400);
        }
        self::validateCategoriaName($data->nomcat);
        $data->nomcat = $data->nomcat ?? null;
        $categoria = $this->categoriaRepository->create($data);

        return $categoria;
    }

    public function update($input, int $codcat)
    {
        $categoria = $this->getOneFromDb($codcat);
        $data = json_decode(json_encode($input), false);
        if (!isset($data->nomcat) && !isset($data->nomcat)) {
            throw new Categoria('Enter the data to update the categoria.', 400);
        }
        if (isset($data->nomcat)) {
            $categoria->nomcat = self::validateCategoriaName($data->nomcat);
        }
       
        $categorias = $this->categoriaRepository->update($categoria);

        return $categorias;
    }

    public function delete(int $codcat): string
    {
        $this->getOneFromDb($codcat);
        $data = $this->categoriaRepository->delete($codcat);
        return $data;
    }
}
