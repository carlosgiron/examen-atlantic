<?php

declare(strict_types=1);

namespace App\Service\Categoria;

use App\Exception\Categoria;
use App\Repository\CategoriaRepository;
use App\Service\BaseService;

use Respect\Validation\Validator as v;

abstract class Base extends BaseService
{
    private const REDIS_KEY = 'categoria:%s';

    // protected CategoriaRepository $categoriaRepository;

    // protected RedisService $redisService;

    public function __construct(CategoriaRepository $categoriaRepository)
    {
        $this->categoriaRepository = $categoriaRepository;
    }

    protected static function validateCategoriaName(string $name): string
    {
        if (!v::length(2, 50)->validate($name)) {
            throw new Categoria('El nombre del categoria es inválido.', 400);
        }
        return $name;
    }
    protected function getOneFromDb(int $categoriaId)
    {
        return $this->categoriaRepository->getCategoria($categoriaId);
    }
}


 
 