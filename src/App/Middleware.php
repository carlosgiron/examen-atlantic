<?php

declare(strict_types=1);
$corsOptions = array(
    "origin" => "*",
    // "exposeHeaders" => array("X-My-Custom-Header", "X-Another-Custom-Header"),
    // "maxAge" => 1728000,
    "allowCredentials" => True,
    // "allowMethods" => array("POST, GET"),
    // "allowHeaders" => array("X-PINGOTHER")
    );
$app->add(new \CorsSlim\CorsSlim($corsOptions));
