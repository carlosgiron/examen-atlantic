<?php

declare(strict_types=1);

$app->get('/', 'App\Controller\DefaultController:getHelp');
$app->get('/status', 'App\Controller\DefaultController:getStatus');

$app->group('/api/v1', function () use ($app): void {
    
    $app->group('/categoria', function () use ($app): void {
        $app->get('', \App\Controller\Categoria\GetAll::class);
        $app->get('/[{id_categoria}]', \App\Controller\Categoria\GetOne::class);
        $app->get('/grupo/[{id_grupo}]', \App\Controller\Categoria\GetPorGrupo::class);
        // $app->get('/search/[{query}]', \App\Controller\Grupo\Search::class);
        $app->post('', \App\Controller\Categoria\Create::class);
        $app->put('/[{id_categoria}]', \App\Controller\Categoria\Update::class);
        $app->delete('/[{id_categoria}]', \App\Controller\Categoria\Delete::class);
    });
    $app->group('/producto', function () use ($app): void {
        $app->get('', \App\Controller\Producto\GetAll::class);
        $app->get('/[{id_producto}]', \App\Controller\Producto\GetOne::class);
        $app->get('/categoria/[{id_categoria}]', \App\Controller\Producto\GetOne::class);
        $app->get('/carrito/[{id_producto}]', \App\Controller\Producto\Carrito::class);
        // $app->get('/search/[{query}]', \App\Controller\Grupo\Search::class);
        $app->post('', \App\Controller\Producto\Create::class);
        $app->put('/[{id_producto}]', \App\Controller\Producto\Update::class);
        $app->delete('/[{id_producto}]', \App\Controller\Producto\Delete::class);
    });
    
});
