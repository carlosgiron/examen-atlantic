<?php

declare(strict_types=1);

 
use App\Service\Categoria\CategoriaService;
 
use App\Service\Producto\ProductoService;
 
use Psr\Container\ContainerInterface;

$container = $app->getContainer();

 
$container['categoria_service'] = static function (ContainerInterface $container): CategoriaService {
    return new CategoriaService($container->get('categoria_repository'));
};
$container['producto_service'] = static function (ContainerInterface $container): ProductoService {
    return new ProductoService($container->get('producto_repository'));
};
 