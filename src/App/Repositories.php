<?php

declare(strict_types=1);

 
use App\Repository\CategoriaRepository;
 
use App\Repository\ProductoRepository;
 
use Psr\Container\ContainerInterface;

$container = $app->getContainer();

 
$container['categoria_repository'] = static function (ContainerInterface $container): CategoriaRepository {
    return new CategoriaRepository($container->get('db'));
};
$container['producto_repository'] = static function (ContainerInterface $container): ProductoRepository {
    return new ProductoRepository($container->get('db'));
}; 