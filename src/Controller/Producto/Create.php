<?php

declare(strict_types=1);

namespace App\Controller\Producto;

use Slim\Http\Request;
use Slim\Http\Response;

final class Create extends Base
{
    public function __invoke(Request $request, Response $response): Response
    {
        $input = $request->getParsedBody();
        if (isset($input)) {
            $producto = $this->getProductoService()->create($input);
            $status = 'success';
            $code = 201;
        } else {
            $producto = null;
            $status = 'error';
            $code = 404;
        }
        return $this->jsonResponse($response,  $status, $producto, $code);
    }
}
