<?php

declare(strict_types=1);

namespace App\Controller\Producto;

use Slim\Http\Request;
use Slim\Http\Response;

final class GetAll extends Base
{
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $producto = $this->getProductoService()->getAll(0, 0);

        return $this->jsonResponse($response, 'success', $producto, 200);
    }
}
