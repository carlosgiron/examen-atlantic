<?php

declare(strict_types=1);

namespace App\Controller\Producto;

use Slim\Http\Request;
use Slim\Http\Response;

final class Update extends Base
{
    public function __invoke(Request $request, Response $response, array $args): Response
    {
      
        if (isset($args['id_producto']) && $args['id_producto'] != 0) {
            $input = $request->getParsedBody();
            $producto = $this->getProductoService()->update($input, (int) $args['id_producto']);
            $status = 'success';
            $code = 200;
        } else {
            $producto = null;
            $status = 'error';
            $code = 404;
        }
        return $this->jsonResponse($response,  $status, $producto, $code);
    }
}
