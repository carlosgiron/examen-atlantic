<?php

declare(strict_types=1);

namespace App\Controller\Producto;

use App\Controller\BaseController;
use App\Service\Producto\ProductoService;
use Slim\Container;

abstract class Base extends BaseController
{
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function getProductoService(): ProductoService
    {
        return $this->container->get('producto_service');
    }
}
