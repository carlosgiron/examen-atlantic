<?php

declare(strict_types=1);

namespace App\Controller\Producto;

use Slim\Http\Request;
use Slim\Http\Response;

final class GetOne extends Base
{
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id_producto']) && isset($args['id_producto']) != 0) {
            $producto = $this->getProductoService()->getOne((int) $args['id_producto']);
            $status = 'success';
            $code = 200;
        } elseif (isset($args['id_categoria']) && isset($args['id_categoria']) != 0) {
            $producto =  $this->getProductoService()->getAll(0, intval($args['id_categoria']));
            $status = 'success';
            $code = 200;
        } else {
            $producto = null;
            $status = 'error';
            $code = 404;
        }

        return $this->jsonResponse($response,  $status, $producto, $code);
    }
}
