<?php

declare(strict_types=1);

namespace App\Controller\Producto;

use Slim\Http\Request;
use Slim\Http\Response;

final class Delete extends Base
{
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id_producto']) && $args['id_producto'] != 0) {
            $input = $request->getParsedBody();
            $productoId = (int) $args['id_producto'];
            $producto = $this->getProductoService()->delete($productoId);
            $status = 'success';
            $code = 200;//204
        } else {
            $producto = null;
            $status = 'error';
            $code = 404;
        }
        return $this->jsonResponse($response,  $status, $producto, $code);
    }
}
