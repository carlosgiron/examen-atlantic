<?php

declare(strict_types=1);

namespace App\Controller\Categoria;

use Slim\Http\Request;
use Slim\Http\Response;

final class GetAll extends Base
{
    public function __invoke(Request $request, Response $response): Response
    {
        $categoria = $this->getCategoriaService()->getAll();

        return $this->jsonResponse($response, 'success', $categoria, 200);
    }
}
