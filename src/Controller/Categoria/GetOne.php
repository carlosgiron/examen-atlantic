<?php

declare(strict_types=1);

namespace App\Controller\Categoria;

use Slim\Http\Request;
use Slim\Http\Response;

final class GetOne extends Base
{
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id_categoria'])) {
            $categoria = $this->getCategoriaService()->getOne((int) $args['id_categoria']);
            $status = 'success';
            $code = 200;
        } else {
            $categoria = null;
            $status = 'error';
            $code = 404;
        }
        return $this->jsonResponse($response,  $status, $categoria, $code);
    }
}
