<?php

declare(strict_types=1);

namespace App\Controller\Categoria;

use Slim\Http\Request;
use Slim\Http\Response;

final class GetPorGrupo extends Base
{
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id_grupo'])) {
            $categoria = $this->getCategoriaService()->getPorGrupo((int) $args['id_grupo']);
            $status = 'success';
            $code = 200;
        } else {
            $categoria = null;
            $status = 'error';
            $code = 404;
        }
        return $this->jsonResponse($response,  $status, $categoria, $code);
    }
}
