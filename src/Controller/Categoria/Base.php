<?php

declare(strict_types=1);

namespace App\Controller\Categoria;

use App\Controller\BaseController;
use App\Service\Categoria\CategoriaService;
use Slim\Container;

abstract class Base extends BaseController
{
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function getCategoriaService(): CategoriaService
    {
        return $this->container->get('categoria_service');
    }
}
