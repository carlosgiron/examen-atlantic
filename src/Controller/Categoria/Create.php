<?php

declare(strict_types=1);

namespace App\Controller\Categoria;

use Slim\Http\Request;
use Slim\Http\Response;

final class Create extends Base
{
    public function __invoke(Request $request, Response $response): Response
    {
        $input = $request->getParsedBody();
        if (isset($input)) {
            $categoria = $this->getCategoriaService()->create($input);
            $status = 'success';
            $code = 201;
        } else {
            $categoria = null;
            $status = 'error';
            $code = 404;
        }
        return $this->jsonResponse($response,  $status, $categoria, $code);
    }
}
