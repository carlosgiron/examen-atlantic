<?php

declare(strict_types=1);

namespace App\Controller\Categoria;

use Slim\Http\Request;
use Slim\Http\Response;

final class Delete extends Base
{
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        if (isset($args['id_categoria']) && $args['id_categoria'] != 0) {
            $input = $request->getParsedBody();
            $categoriaId = (int) $args['id_categoria'];
            $categoria = $this->getCategoriaService()->delete($categoriaId);
            $status = 'success';
            $code = 200;//204
        } else {
            $categoria = null;
            $status = 'error';
            $code = 404;
        }
        return $this->jsonResponse($response,  $status, $categoria, $code);
    }
}
