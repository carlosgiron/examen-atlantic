<?php

declare(strict_types=1);

namespace App\Controller;

use Slim\Container;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

final class UploadFile extends BaseController
{
    public const API_VERSION = '0.55.0';

    public function __construct(Container $container)
    {
        $this->container = $container;
    }


    public function postUploadFile(Request $request, Response $response): Response
    {
        $url = getenv('APP_DOMAIN');
        // $directory = $this->get('upload_directory');
        $directory = __DIR__ . '/../../public/upload_files/';

        $uploadedFiles = $request->getUploadedFiles();
        // handle single input with single file upload
        $uploadedFile =  $uploadedFiles['file'];
        // $total = count($uploadedFiles['name']);
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = $this->moveUploadedFile($directory, $uploadedFile);
            // $response->write('uploaded ' . $filename . '<br/>');
            $result = [
                'status' => "success",
                'originalName' => $filename['originalName'],
                'generatedName' => $filename['generatedName'],
                'filePath' => $filename['filePath'],
                'extension' => $filename['extension']
            ];
        } else {
            $filename = $uploadedFiles['profile'];
            $result = [
                'status' => "error",
                'message' => 'El directorio de destino no se puede escribir.',
                'filePath' => $filename['filePath'],
                'extension' => $filename['extension']
            ];
        }
        // echo json_encode($result);
        // $message = [
        //     // 'endpoints' => $endpoints,
        //     'version' => self::API_VERSION,
        //     'timestamp' => time(),
        // ];

        return $this->jsonResponse($response, 'success', $result, 200);
    }


    function moveUploadedFile($directory, UploadedFile $uploadedFile)
    {
        $originalName = $uploadedFile->getClientFilename();
        $ext = '.' . pathinfo($originalName, PATHINFO_EXTENSION);
        $ext2 = pathinfo($originalName, PATHINFO_EXTENSION);
        switch ($ext2) {
            case "jpg":
                $folder = 'images/';
                break;
            case "png":
                $folder = 'images/';
                break;
            case "gif":
                $folder = 'images/';
                break;
            case "jpeg":
                $folder = 'images/';
                break;
            case "webp":
                $folder = 'images/';
                break;
            case "mp4":
                $folder = 'videos/';
                break;
            case "webm":
                $folder = 'videos/';
                break;
            case "ogv":
                $folder = 'videos/';
                break;

            case "": // Handle file extension for files ending in '.'
            case NULL: // Handle no file extension
                break;
            default:
                $folder = '';
        }

        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);


        $directory = $directory . $folder;
        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
        $return_ = [
            'originalName' => $originalName,
            'generatedName' => $filename,
            'filePath' =>  'upload_files/' . $folder . $filename,
            'extension' => $ext2
        ];
        return $return_;
    }
}
