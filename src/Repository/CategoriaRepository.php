<?php

declare(strict_types=1);

namespace App\Repository;

use App\Exception\Categoria;

final class CategoriaRepository extends BaseRepository
{
    public function __construct(\PDO $database)
    {
        $this->database = $database;
    }

    public function getCategoria(int $codcat): object
    {
        $query = 'call usp_lista_categoria_id(?)';

        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $codcat);
        $statement->execute();
        $categoria = $statement->fetchObject();
        if (!$categoria) {
            throw new Categoria('Categoria no encontrado.', 404);
        }

        return $categoria;
    }
    public function getPorGrupo(int $idGrupo): array
    {
        $query = 'call usp_lista_categoria(?)';

        $statement = $this->database->prepare($query);
        $codcat = 0;
        $statement->bindParam(1, $codcat);
    
        $statement->execute();
        return $statement->fetchAll();
    }
    public function getAll(): array
    {
        $query = 'select * from categoria';
        $statement = $this->database->prepare($query);
      
        // $statement->bindParam(1, $codcat);
        $statement->execute();

        return $statement->fetchAll();
    }


    public function create(object $categoria): object
    {
        $query = 'SELECT ufn_inserta_categoria(?,?,?,?) as cod_respuesta';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $categoria->nom_categoria);
        $statement->bindParam(2, $categoria->nom_descripcion);
        $statement->bindParam(3, $categoria->nom_imagen);
        $statement->bindParam(4, $categoria->id_grupo);
        $statement->execute();
        $resultStat = $statement->fetchAll();
        $id = $resultStat[0]["cod_respuesta"];
        return $this->getCategoria((int) $id);
    }

    public function update(object $categoria): object
    {
        $query = 'SELECT ufn_actualiza_categoria(?,?,?,?,?)';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $categoria->id_categoria);
        $statement->bindParam(2, $categoria->nom_categoria);
        $statement->bindParam(3, $categoria->nom_descripcion);
        $statement->bindParam(4, $categoria->nom_imagen);
        $statement->bindParam(5, $categoria->id_grupo);
        $statement->execute();
        $result = $this->getCategoria((int) $categoria->id_categoria);
        return $result;
    }

    public function delete(int $categoriaId): string
    {
        $query = 'SELECT ufn_elimina_categoria(?)';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $categoriaId);
        $statement->execute();

        return 'categoría fue eliminado.';
    }
}
