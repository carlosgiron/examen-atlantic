<?php

declare(strict_types=1);

namespace App\Repository;

use App\Exception\Producto;

final class ProductoRepository extends BaseRepository
{
    public function __construct(\PDO $database)
    {
        $this->database = $database;
    }

    public function getProducto(int $codpro): object
    {
        $query = 'call usp_lista_producto(?)';
        $statement = $this->database->prepare($query);
   
        $statement->bindParam(1, $codpro);
      
        $statement->execute();
        $user = $statement->fetchObject();
        if (!$user) {
            throw new Producto('Producto no encontrado.', 404);
        }

        return $user;
    }



    public function getAll(int $codpro ): array
    {
        $query = 'call usp_lista_producto(?)';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $codpro);
 
        $statement->execute();

        return $statement->fetchAll();
    }
    /*
    public function search(string $usersName): array
    {
        $query = 'SELECT `id`, `name`, `email` FROM `users` WHERE `name` LIKE :name ORDER BY `id`';
        $name = '%' . $usersName . '%';
        $statement = $this->database->prepare($query);
        $statement->bindParam('name', $name);
        $statement->execute();
        $users = $statement->fetchAll();
        if (!$users) {
            throw new User('User name not found.', 404);
        }

        return $users;
    }
 
   */

    public function create(object $producto): object
    {
        $query = 'SELECT ufn_inserta_producto(?,?)';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $producto->nompro);
        $statement->bindParam(2, $producto->precio);
        $statement->execute();

        return $this->getProducto((int) $this->database->lastInsertId());
    }

    public function update(object $producto): object
    {
        $query = 'SELECT ufn_actualiza_producto(?,?,?)';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $producto->codpro);
        $statement->bindParam(2, $producto->nompro);
        $statement->bindParam(3, $producto->precio);
        $statement->execute();
        $result = $this->getProducto((int) $producto->codpro);
        return $result;
    }

    public function delete(int $codpro): string
    {
        $query = 'SELECT ufn_elimina_producto(?)';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $codpro);
        $statement->execute();

        return 'Producto fue eliminado.';
    }

    public function getCarrito(string $codpros): array
    {
        $query = 'call usp_lista_producto_carrito(?)';
        $statement = $this->database->prepare($query);
        $statement->bindParam(1, $codpros);
        $statement->execute();

        return $statement->fetchAll();
    }
}
