<?php

declare(strict_types = 1);

namespace App\Helpers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

final class OtrosHelpers {

    // para verificar si un string esta compuesto de solo numeros sin comas ni puntos
    public function soloNumeros($laCadena) {
        $carsValidos = "0123456789";
        for ($i = 0; $i < strlen($laCadena); $i++) {
            if (strpos($carsValidos, substr($laCadena, $i, 1)) === false) {
                return false;
            }
        }
        return true;
    }

    public function edad($fecha) {
        $fecha = str_replace("/", "-", $fecha);
        $fecha = date('Y/m/d', strtotime($fecha));
        $hoy = date('Y/m/d');
        $edad = $hoy - $fecha;
        return $edad;
    }

    public function strtoupper_total($string) {
        return strtr(strtoupper($string), "àèìòùáéíóúçñäëïöü", "ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ");
    }

    public function strtolower_total($string) {
        return strtr(strtolower($string), "ÀÈÌÒÙÁÉÍÓÚÇÑÄËÏÖÜ", "àèìòùáéíóúçñäëïöü");
    }

    public function comprobar_email($email) {
        $mail_correcto = FALSE;
        //compruebo unas cosas primeras 
        if ((strlen($email) >= 6) && (substr_count($email, "@") == 1) && (substr($email, 0, 1) != "@") && (substr($email, strlen($email) - 1, 1) != "@")) {
            if ((!strstr($email, "'")) && (!strstr($email, "\"")) && (!strstr($email, "\\")) && (!strstr($email, "\$")) && (!strstr($email, " "))) {
                //miro si tiene caracter . 
                if (substr_count($email, ".") >= 1) {
                    //obtengo la terminacion del dominio 
                    $term_dom = substr(strrchr($email, '.'), 1);
                    //compruebo que la terminación del dominio sea correcta 
                    if (strlen($term_dom) > 1 && strlen($term_dom) < 5 && (!strstr($term_dom, "@"))) {
                        //compruebo que lo de antes del dominio sea correcto 
                        $antes_dom = substr($email, 0, strlen($email) - strlen($term_dom) - 1);
                        $caracter_ult = substr($antes_dom, strlen($antes_dom) - 1, 1);
                        if ($caracter_ult != "@" && $caracter_ult != ".") {
                            $mail_correcto = 1;
                        }
                    }
                }
            }
        }
        return $mail_correcto;
    }

    public function enviar_mail($asunto, $setFromAleas, $cuerpo, $listaDestinatarios, $listaAdjuntos = FALSE) {
        try {

            date_default_timezone_set('UTC');

            $mail = new PHPMailer(true);
            $mail->IsSMTP(true);
            $mail->SMTPAuth = true;
            $mail->SMTPDebug = false;
            $mail->Host = getenv('SMTP_HOST');
            $mail->Port = getenv('SMTP_PORT');
            $mail->Username = getenv('SMTP_USERNAME');
            $mail->Password = getenv('SMTP_PASSWORD');
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            $mail->SetFrom(getenv('SET_FROM'), $setFromAleas);
            // $mail->AddReplyTo("ernesto.mogrovejo@gmail.com", $setFromAleas);
            $mail->setLanguage('es', '/libreries/PHPMailer/language/directory/');
            $mail->Subject = $asunto;
            $mail->IsHTML(true);
            $mail->AltBody = $cuerpo;
            $mail->MsgHTML($cuerpo);
            $mail->CharSet = 'UTF-8';

            if ($listaAdjuntos) {
                foreach ($listaAdjuntos as $key => $fileUrl) {
                    $fichero = file_get_contents($fileUrl);
                    // $mail->addStringAttachment($fichero, 'comprobanteCita.pdf');
                }
            }
            //print_r($mail);
            $response = array();
            // array_push($listaDestinatarios, 'micorreo@gmail.com');
            array_push($listaDestinatarios);
            foreach ($listaDestinatarios as $key => $email) {
                if ($email != null && $email != '') {
                    if ($this->comprobar_email($email)) {
                        $mail->AddAddress($email);
                        
                    } else {
                        array_push(
                                $response, array(
                            'flag' => 2,
                            'msgMail' => 'Notificación de correo NO enviada. Correo invalido.'
                                )
                        );
                    }
                } else {
                    array_push(
                            $response, array(
                        'flag' => 3,
                        'msgMail' => 'Notificación de correo NO enviada. Correo no registrado.'
                            )
                    );
                }
            }
            //
            if ($mail->Send()) {
                array_push(
                        $response, array(
                    'flag' => 1,
                    'msgMail' => 'Notificación de correo enviada exitosamente.'
                        )
                );
            } else {
                $mail->ErrorInfo;
                array_push(
                        $response, array(
                    'flag' => 0,
                    'msgMail' => 'Notificación de correo NO enviada.'
                        )
                );
            }

            return $response;
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
    function stripAccents($stripAccents){
        return strtr($stripAccents,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
      }
}
